<?php

/**
 * Covers filter form base class.
 *
 * @package    spalah
 * @subpackage filter
 * @author     Your name here
 */
abstract class BaseCoversFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'name_show'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'size'       => new sfWidgetFormFilterInput(),
      'height'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'width'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'hash'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'content'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'updated_at' => new sfWidgetFormFilterInput(),
      'created_at' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'       => new sfValidatorPass(array('required' => false)),
      'name_show'  => new sfValidatorPass(array('required' => false)),
      'size'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'height'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'width'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'hash'       => new sfValidatorPass(array('required' => false)),
      'content'    => new sfValidatorPass(array('required' => false)),
      'updated_at' => new sfValidatorPass(array('required' => false)),
      'created_at' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('covers_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Covers';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'name'       => 'Text',
      'name_show'  => 'Text',
      'size'       => 'Number',
      'height'     => 'Number',
      'width'      => 'Number',
      'hash'       => 'Text',
      'content'    => 'Text',
      'updated_at' => 'Text',
      'created_at' => 'Text',
    );
  }
}
