<?php

/**
 * EventsDate filter form base class.
 *
 * @package    spalah
 * @subpackage filter
 * @author     Your name here
 */
abstract class BaseEventsDateFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'Covers_id'  => new sfWidgetFormPropelChoice(array('model' => 'Covers', 'add_empty' => true)),
      'name'       => new sfWidgetFormFilterInput(),
      'content'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'link'       => new sfWidgetFormFilterInput(),
      'date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'Covers_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Covers', 'column' => 'id')),
      'name'       => new sfValidatorPass(array('required' => false)),
      'content'    => new sfValidatorPass(array('required' => false)),
      'link'       => new sfValidatorPass(array('required' => false)),
      'date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'created_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('events_date_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EventsDate';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'Covers_id'  => 'ForeignKey',
      'name'       => 'Text',
      'content'    => 'Text',
      'link'       => 'Text',
      'date'       => 'Date',
      'created_at' => 'Date',
      'updated_at' => 'Date',
    );
  }
}
