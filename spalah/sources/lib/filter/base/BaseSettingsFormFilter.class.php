<?php

/**
 * Settings filter form base class.
 *
 * @package    spalah
 * @subpackage filter
 * @author     Your name here
 */
abstract class BaseSettingsFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'param'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'descripon' => new sfWidgetFormFilterInput(),
      'value'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'param'     => new sfValidatorPass(array('required' => false)),
      'descripon' => new sfValidatorPass(array('required' => false)),
      'value'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('settings_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Settings';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'param'     => 'Text',
      'descripon' => 'Text',
      'value'     => 'Text',
    );
  }
}
