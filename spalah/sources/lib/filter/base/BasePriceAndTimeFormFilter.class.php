<?php

/**
 * PriceAndTime filter form base class.
 *
 * @package    spalah
 * @subpackage filter
 * @author     Your name here
 */
abstract class BasePriceAndTimeFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'price'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'time_open'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'time_clouse' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'price'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'time_open'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'time_clouse' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('price_and_time_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PriceAndTime';
  }

  public function getFields()
  {
    return array(
      'price'       => 'Number',
      'time_open'   => 'Number',
      'time_clouse' => 'Number',
      'id'          => 'Number',
    );
  }
}
