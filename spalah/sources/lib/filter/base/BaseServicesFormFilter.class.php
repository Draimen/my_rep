<?php

/**
 * Services filter form base class.
 *
 * @package    spalah
 * @subpackage filter
 * @author     Your name here
 */
abstract class BaseServicesFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'covorking' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'event'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'play_xbox' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'covorking' => new sfValidatorPass(array('required' => false)),
      'event'     => new sfValidatorPass(array('required' => false)),
      'play_xbox' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('services_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Services';
  }

  public function getFields()
  {
    return array(
      'covorking' => 'Text',
      'event'     => 'Text',
      'play_xbox' => 'Text',
      'id'        => 'Number',
    );
  }
}
