<?php

/**
 * Feedback filter form base class.
 *
 * @package    spalah
 * @subpackage filter
 * @author     Your name here
 */
abstract class BaseFeedbackFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'email' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'phone' => new sfWidgetFormFilterInput(),
      'name'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'email' => new sfValidatorPass(array('required' => false)),
      'phone' => new sfValidatorPass(array('required' => false)),
      'name'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('feedback_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Feedback';
  }

  public function getFields()
  {
    return array(
      'id'    => 'Number',
      'email' => 'Text',
      'phone' => 'Text',
      'name'  => 'Text',
    );
  }
}
