<?php

/**
 * EventsDate form.
 *
 * @package    spalah
 * @subpackage form
 * @author     Your name here
 */
class EventsDateForm extends BaseEventsDateForm
{
  public function configure()
  {
       unset(
      $this['created_at'], $this['updated_at']
    );
       $this->widgetSchema['Covers_id'] = new sfWidgetFormInputFileEditable(array(
           'edit_mode'  => false == $this->isNew,
           'is_image'   => true,
           'file_src'   => $this->getImagePath(),
           'with_delete'=> $this->getWithDelete(),
       ));
 

       
       $this->validatorSchema['Covers_id'] = new sfValidatorPass(array('required' => false));
  }
  protected function getImagePath()
    {
        if (false == $this->isNew) {
            if ($this->getObject()->getCoversId()) 
            {
                  return '/uploads/100/100/' . $this->getObject()->getCovers()->getHash();
            }

        }

        return '';
    }
    protected function getWithDelete()
    {
        if (false == $this->isNew) {

            if ($this->getObject()->getCoversId()) {
                return true;
            }

        }

        return false;
    }
   
     public function doSaveMedia($v)
    {
        $content = file_get_contents($v['tmp_name']);
        $hash = md5($content);
        $size = getimagesize($v['tmp_name']);
        
        $image = new Imagick();
        $image->readImageBlob($content);
        $image->scaleImage(100, 100);
        $content = $image->getImage();
        $this->getResponse()->setContentType('image/png');
    //   var_dump($size); 
        $criteria = new Criteria;
        $criteria->add(CoversPeer::HASH, $hash);
        $obj = CoversPeer::doSelectOne($criteria);

        try {
            if (empty($obj)) {
                
                $obj = new Covers;
                $obj->setName($v['name'])
                    ->setNameShow($v['name'])
                    ->setSize($v['size'])
                    ->setHeight($size[2])
                    ->setWidth($size[1])
                    ->setHash($hash)
                    ->setContent(base64_encode($content))
                    ->setCreatedAt(date("Y-m-d H:i"))
                    ->setUpdatedAt(date("Y-m-d H:i"))
                    ->save();
                
            }
        } catch (Exception $e) {
            
           echo "stdfdfdfdfrt";die;
        }
       $criteria = new Criteria;
        $criteria->add(CoversPeer::HASH, $obj->getHash());
        $obj = CoversPeer::doSelectOne($criteria);
        
        var_dump($obj->getId());
        return $obj->getId();
    }
      public function processValues($values) {
    
         
         $values['Covers_id']=$this->doSaveMedia($values['Covers_id']);
        
         var_dump($values);
        return $values;
    }
}
