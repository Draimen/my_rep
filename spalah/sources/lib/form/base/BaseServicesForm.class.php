<?php

/**
 * Services form base class.
 *
 * @method Services getObject() Returns the current form's model object
 *
 * @package    spalah
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseServicesForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'covorking' => new sfWidgetFormTextarea(),
      'event'     => new sfWidgetFormTextarea(),
      'play_xbox' => new sfWidgetFormTextarea(),
      'id'        => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'covorking' => new sfValidatorString(),
      'event'     => new sfValidatorString(),
      'play_xbox' => new sfValidatorString(),
      'id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('services[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Services';
  }


}
