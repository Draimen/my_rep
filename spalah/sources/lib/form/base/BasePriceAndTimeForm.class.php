<?php

/**
 * PriceAndTime form base class.
 *
 * @method PriceAndTime getObject() Returns the current form's model object
 *
 * @package    spalah
 * @subpackage form
 * @author     Your name here
 */
abstract class BasePriceAndTimeForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'price'       => new sfWidgetFormInputText(),
      'time_open'   => new sfWidgetFormInputText(),
      'time_clouse' => new sfWidgetFormInputText(),
      'id'          => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'price'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'time_open'   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'time_clouse' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('price_and_time[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'PriceAndTime';
  }


}
