<?php

/**
 * EventsDate form base class.
 *
 * @method EventsDate getObject() Returns the current form's model object
 *
 * @package    spalah
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseEventsDateForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'Covers_id'  => new sfWidgetFormPropelChoice(array('model' => 'Covers', 'add_empty' => false)),
      'name'       => new sfWidgetFormInputText(),
      'content'    => new sfWidgetFormTextarea(),
      'link'       => new sfWidgetFormInputText(),
      'date'       => new sfWidgetFormDateTime(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'Covers_id'  => new sfValidatorPropelChoice(array('model' => 'Covers', 'column' => 'id')),
      'name'       => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'content'    => new sfValidatorString(),
      'link'       => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'date'       => new sfValidatorDateTime(),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorPropelUnique(array('model' => 'EventsDate', 'column' => array('date')))
    );

    $this->widgetSchema->setNameFormat('events_date[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'EventsDate';
  }


}
