<?php

/**
 * Feedback form base class.
 *
 * @method Feedback getObject() Returns the current form's model object
 *
 * @package    spalah
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseFeedbackForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'    => new sfWidgetFormInputHidden(),
      'email' => new sfWidgetFormInputText(),
      'phone' => new sfWidgetFormInputText(),
      'name'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'    => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'email' => new sfValidatorString(array('max_length' => 45)),
      'phone' => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'name'  => new sfValidatorString(array('max_length' => 45)),
    ));

    $this->widgetSchema->setNameFormat('feedback[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Feedback';
  }


}
