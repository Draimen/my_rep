<?php

/**
 * Covers form base class.
 *
 * @method Covers getObject() Returns the current form's model object
 *
 * @package    spalah
 * @subpackage form
 * @author     Your name here
 */
abstract class BaseCoversForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'name'       => new sfWidgetFormInputText(),
      'name_show'  => new sfWidgetFormInputText(),
      'size'       => new sfWidgetFormInputText(),
      'height'     => new sfWidgetFormInputText(),
      'width'      => new sfWidgetFormInputText(),
      'hash'       => new sfWidgetFormInputText(),
      'content'    => new sfWidgetFormTextarea(),
      'updated_at' => new sfWidgetFormInputText(),
      'created_at' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->getId()), 'empty_value' => $this->getObject()->getId(), 'required' => false)),
      'name'       => new sfValidatorString(array('max_length' => 45)),
      'name_show'  => new sfValidatorString(array('max_length' => 45)),
      'size'       => new sfValidatorNumber(array('required' => false)),
      'height'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'width'      => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
      'hash'       => new sfValidatorString(array('max_length' => 45)),
      'content'    => new sfValidatorString(),
      'updated_at' => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'created_at' => new sfValidatorString(array('max_length' => 45, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('covers[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Covers';
  }


}
