<?php use_helper('I18N', 'Date') ?>
<?php include_partial('feedback/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('New Feedback', array(), 'messages') ?></h1>

  <?php include_partial('feedback/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('feedback/form_header', array('Feedback' => $Feedback, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('feedback/form', array('Feedback' => $Feedback, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('feedback/form_footer', array('Feedback' => $Feedback, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
