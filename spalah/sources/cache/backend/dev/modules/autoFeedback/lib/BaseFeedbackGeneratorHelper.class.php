<?php

/**
 * feedback module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage feedback
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseFeedbackGeneratorHelper extends sfModelGeneratorHelper
{
  public function getUrlForAction($action)
  {
    return 'list' == $action ? 'feedback' : 'feedback_'.$action;
  }
}
