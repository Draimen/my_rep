<td class="sf_admin_text sf_admin_list_td_id">
  <?php echo link_to($EventsDate->getId(), 'events_date_edit', $EventsDate) ?>
</td>
<td class="sf_admin_foreignkey sf_admin_list_td_Covers_id">
  <?php echo $EventsDate->getCoversId() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_name">
  <?php echo $EventsDate->getName() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_content">
  <?php echo $EventsDate->getContent() ?>
</td>
<td class="sf_admin_text sf_admin_list_td_link">
  <?php echo $EventsDate->getLink() ?>
</td>
<td class="sf_admin_date sf_admin_list_td_date">
  <?php echo false !== strtotime($EventsDate->getDate()) ? format_date($EventsDate->getDate(), "f") : '&nbsp;' ?>
</td>
<td class="sf_admin_date sf_admin_list_td_created_at">
  <?php echo false !== strtotime($EventsDate->getCreatedAt()) ? format_date($EventsDate->getCreatedAt(), "f") : '&nbsp;' ?>
</td>
<td class="sf_admin_date sf_admin_list_td_updated_at">
  <?php echo false !== strtotime($EventsDate->getUpdatedAt()) ? format_date($EventsDate->getUpdatedAt(), "f") : '&nbsp;' ?>
</td>
