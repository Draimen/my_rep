<?php

/**
 * events module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage events
 * @author     ##AUTHOR_NAME##
 */
abstract class BaseEventsGeneratorHelper extends sfModelGeneratorHelper
{
  public function getUrlForAction($action)
  {
    return 'list' == $action ? 'events_date' : 'events_date_'.$action;
  }
}
