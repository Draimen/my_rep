<?php

/**
 * image actions.
 *
 * @package    sf_sandbox
 * @subpackage image
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class imageActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
    }

    public function executeTest() {
        $file = sfConfig::get('sf_root_dir') . '/web/images/bg.png';
        $content = file_get_contents($file);

        $hash = md5($content);

        try {
            $obj = new Covers;
            $obj->setName($hash)
                    ->setContent(base64_encode($content))
                    ->save();
        } catch (Exception $e) {
            $criteria = new Criteria;
            $criteria->add(MediaPeer::NAME, $hash);
            $obj = MediaPeer::doSelectOne($criteria);
        }
    }

    public function executeShow(sfWebRequest $request) {
        $hash = $request->getParameter('hash');

        $criteria = new Criteria;
        $criteria->add(MediaPeer::NAME, $hash);
        $obj = MediaPeer::doSelectOne($criteria);

        $content = base64_decode($obj->getContent());
        $this->getResponse()->setContentType('image/png');

        file_put_contents(sfConfig::get('sf_root_dir') . '/web/uploads/' . $hash, $content);

        return $this->renderText($content);
    }

    public function executeResize(sfWebRequest $request) {
        $hash = $request->getParameter('hash');
        $x = $request->getParameter('x');
        $y = $request->getParameter('y');
        $criteria = new Criteria;
        $criteria->add(CoversPeer::hash, $hash);
        $obj = CoversPeer::doSelectOne($criteria);

        $content = base64_decode($obj->getContent());

        $image = new Imagick();
        $image->readImageBlob($content);
        $image->scaleImage($x, $y);
        $content = $image->getImage();
        $this->getResponse()->setContentType('image/png');

        $dir = sfConfig::get('sf_root_dir') . '/web/uploads/' . $x . '/' . $y . '/';
        mkdir($dir, 0777, true);
        file_put_contents($dir . $hash, $content);

        return $this->renderText($content);
    }
}
