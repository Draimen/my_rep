<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body>
      <div class="blog-masthead">
            <div class="container">
                <nav class="blog-nav">
                    <a class="blog-nav-item" href="<?php echo url_for('@homepage') ?>">События</a>
                    <a class="blog-nav-item" href="<?php echo url_for('services/index') ?>">Сервисы</a>
                    <a class="blog-nav-item" href="<?php echo url_for('feedback/index') ?>">Обратная связь</a>
                    <a class="blog-nav-item" href="<?php echo url_for('tags/index') ?>">Теги</a>
                     
                </nav>
            </div>
        </div>
    <?php echo $sf_content ?>
  </body>
</html>
