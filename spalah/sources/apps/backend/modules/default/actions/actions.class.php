<?php

/**
 * default actions.
 *
 * @package    spalah
 * @subpackage default
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class defaultActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
  public function executeLogin(sfWebRequest $request) {
        if ($this->getUser()->isAuthenticated()) {
            $this->redirect('default/index');
        }


        if ($request->isMethod('post')) {
            $l = $request->getParameter('l');
            $p = $request->getParameter('p');

            echo $l;
            echo $p;
            $c = new Criteria;
            $c->add(UsersPeer::LOGIN, $l);
            $c->add(UsersPeer::PASSWORD, $p);
            $user = UsersPeer::doSelectOne($c);
        
            if (false == empty($user)) {
               
                $this->getUser()->setAuthenticated(true);
                $this->getUser()->addCredentials(array('admin'));
                $this->getUser()->setAttribute('user', $user);
                $this->redirect('default/index');
            }

        }
    }
}
